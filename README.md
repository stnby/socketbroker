# SocketBroker

## Usage
```
socketbroker -m 10 1234 data.aishub.net 5060
```

All of the data will be forwarded to whoever who is connected to `localhost:1234`.
