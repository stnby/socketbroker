CC=gcc
SRCS = $(wildcard *.c)

all: socketbroker

socketbroker: $(SRCS)
	$(CC) $(CFLAGS) $(SRCS) -o $@

clean:
	rm -f socketbroker
