/*
 * SocketBroker v0.1.1
 * Copyright (C) 2020 Kernal Community, Laurynas Četyrkinas <stnby@kernal.eu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <stdio.h>
#include <errno.h>
#include <netdb.h> /* h_errno, hostent, gethostbyname() */
#include <stdarg.h> /* va_list, ... */
#include <string.h> /* memset() */
#include <unistd.h> /* close() */
#include <stdlib.h> /* exit() */
#include <getopt.h> /* optin, optarg, getopt_long() */
#include <arpa/inet.h> /* inet_ntoa() */
#include <sys/socket.h> /* sockaddr_in, socket(), bind(), connect(), accept() */
#include <sys/select.h> /* fd_set, select(), FD_ZERO(), FD_SET(), FD_ISSET() */

/* Configuration. */
#define DEFAULT_CONNECTION_LIMIT 100
#define BUF_SIZE 128

#define APPLICATION_NAME "socketbroker"
#define MAX_PORT 65535
#define BACKLOG 10

void print_log(const char *fmt, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", APPLICATION_NAME);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fflush(stderr);
}

void die(const char *fmt, ...)
{
	print_log(fmt);
	exit(EXIT_FAILURE);
}


static void print_help()
{
	printf("Usage: %s [options] [listen_port] [hostname] [port]\n", APPLICATION_NAME);
	puts(
"  -h, --help             Display this help screen.\n"
"  -m, --max-conns <n>    Maximum <n> simultaneous connections.\n"
"  -s, --send-only        Only send data to clients.\n"
"  -r, --recv-only        Only receive data from clients."
	);
}

unsigned parse_port(char *str)
{
	char *tail;
	int next;
	errno = 0;
	unsigned port = strtoul(str, &tail, 10);
	if (errno || port < 1 || port > MAX_PORT)
		die("Invalid port number.\n");
	return port;
}

unsigned parse_connlimit(char *str)
{
	char *tail;
	int next;
	errno = 0;
	unsigned conn_limit = strtoul(str, &tail, 10);
	if (errno || conn_limit < 1 || conn_limit >= FD_SETSIZE)
		die("Invalid connection limit.\n");
	return conn_limit;
}

int connect_to(const char *hostname, unsigned port)
{
	int sockfd;
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	struct hostent *hostinfo;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}

	if ((hostinfo = gethostbyname(hostname)) == NULL)
		die("Could not resolve hostname \"%s\".\n", hostname);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr = *(struct in_addr *)hostinfo->h_addr;

	if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
		perror("connect");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

int listener(unsigned port)
{
	int sockfd;
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	int option = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(int));

	if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

int main(int argc, char *argv[])
{
	unsigned buf_size = BUF_SIZE;
	unsigned conn_limit = DEFAULT_CONNECTION_LIMIT;
	int mode = 1;

	struct option long_options[] = {
		{"help", no_argument, NULL, 'h'},
		{"max-conns", required_argument, NULL, 'm'},
		{"send-only", no_argument, NULL, 's'},
		{"recv-only", no_argument, NULL, 'r'},
		{0, 0, 0, 0}
	};

	/* Handle command line arguments. */
	while (1) {
		int option_index;
		int c = getopt_long(argc, argv, "hm:sr", long_options, &option_index);

		/* The end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			print_help();
			return EXIT_SUCCESS;
		case 'm':
			conn_limit = parse_connlimit(optarg);
			break;
		case 's':
			(mode == 1) ? mode = 2 : die("Choose either send or receive only.\n");
			break;
		case 'r':
			(mode == 1) ? mode = 0 : die("Choose either send or receive only.\n");
			break;
		case '?':
			die("Try `--help' for more information and usage options.\n");
		default:
			die("Unrecognised option.\n");
		}
	}
	if (argc - optind < 3)
		die("You must specify a port to listen on and a host to connect to.\n");

	unsigned out_port = parse_port(argv[optind++]);
	char *hostname = argv[optind++];
	unsigned in_port = parse_port(argv[optind]);

	int out_sockfd, in_sockfd, conn_fd;
	int client_sockfd[conn_limit];
	memset(client_sockfd, -1, sizeof(int) * conn_limit);

	in_sockfd = connect_to(hostname, in_port);
	out_sockfd = listener(out_port);

	struct sockaddr_in cli_addr;
	int addrlen = sizeof(struct sockaddr_in);

	char buf[buf_size]; /* Data buffer for sending & receiving. */
	ssize_t bytes_read;
	fd_set fdset;

	while (1) {
		FD_ZERO(&fdset);
		FD_SET(in_sockfd, &fdset);
		FD_SET(out_sockfd, &fdset);
		unsigned maxfd = out_sockfd;

		/* Add all socket descriptors to the read list. */
		for (unsigned i = 0; i < conn_limit; i++) {
			if (client_sockfd[i] == -1)
				continue;
			FD_SET(client_sockfd[i], &fdset);

			/* Find highest file descriptor, needed for the select function. */
			if (client_sockfd[i] > maxfd)
				maxfd = client_sockfd[i];
		}

		/* Wait for traffic. */
		if (select(maxfd + 1, &fdset, NULL, NULL, NULL) == -1) {
			perror("select");
			return EXIT_FAILURE;
		}

		/* IO operations on the out_sockfd. (new client) */
		if (FD_ISSET(out_sockfd, &fdset)) {
			if ((conn_fd = accept(out_sockfd, (struct sockaddr *)&cli_addr,
			    (socklen_t*)&addrlen)) == -1) {
				perror("accept");
				continue;
			}
			print_log("New connection, sockfd: %d, ipaddr: %s, port: %d\n",
			    conn_fd, inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

			int success = 0;
			for (unsigned i = 0; i < conn_limit; i++) {
				/* Check if position is unpopulated. */
				if (client_sockfd[i] == -1) {
					client_sockfd[i] = conn_fd;
					success = 1;
					break;
				}
			}
			if (!success) {
				print_log("Too many clients, connection closed.\n");
				close(conn_fd);
			}
		}

		/* IO operations on the in_sockfd. (data coming in) */
		else if (FD_ISSET(in_sockfd, &fdset)) {
			if ((bytes_read = read(in_sockfd, buf, buf_size)) == -1) {
				perror("read");
				return EXIT_FAILURE;
			}
			/* We have read some data. */
			else if (bytes_read > 0) {
				if (!(mode > 0))
					continue;
				//write(0, buf, bytes_read);
				/* Send it out to all connected clients. */
				for (unsigned i = 0; i < conn_limit; i++) {
					if (client_sockfd[i] == -1)
						continue;
					if (write(client_sockfd[i], buf, bytes_read) == -1) {
						perror("write");
						continue;
					}
				}
			}
			/* Socket in_sockfd disconnected. */
			else
				die("Incoming connection lost.\n");
		}

		/* IO operations on the client sockets. */
		else {
			for (unsigned i = 0; i < conn_limit; i++) {
				if (client_sockfd[i] != -1 && FD_ISSET(client_sockfd[i], &fdset)) {
					if ((bytes_read = read(client_sockfd[i], buf, buf_size)) == -1) {
						perror("read");
						continue;
					}
					/* We have read some data. */
					if (bytes_read > 0) {
						if (mode < 2 && write(in_sockfd, buf, bytes_read) == -1) {
							perror("write");
							continue;
						}
					}
					/* Client socket disconnected. */
					else {
						print_log("Client disconnected.\n");
						close(client_sockfd[i]);
						client_sockfd[i] = -1;
					}
				}
			}
		}
	}
	return EXIT_SUCCESS;
}
